const data = require('../core/data.json')
const DelayedAnimation = require('./components/delayedAnimation.js')

Vue.component("DelayedAnimation", DelayedAnimation);

new Vue({
  el: '#app',
  data () {
    return {
      step: 'one',
      activeState: 'skin',

      // JSON.parse(localStorage.getItem('character')) ||

      character: {
        gender: 'girl',
        name: '',
        skinColors: data.defaultOptions.skinColor,
        hairColors: data.defaultOptions.hairColor,
        hairTypes: data.defaultOptions.hairType,
        accessoryTypes: data.defaultOptions.accessoryType
      },

      skinColors: data.skinColors,
      accessoryTypes: data.accessoryTypes,
      hairTypes: data.hairTypes,
      hairColors: data.hairColors
    }
  },
  computed: {
    defaultImageGirl () {
      return `img/hero/girl/no_jersey/${this.character.skinColors}/${this.character.hairColors}-assets/${this.character.skinColors}${this.character.accessoryTypes}${this.character.hairTypes}${this.character.hairColors}.png`
    },
    defaultImageBoy () {
      return `img/hero/girl/no_jersey/${this.character.skinColors}/${this.character.hairColors}-assets/${this.character.skinColors}${this.character.accessoryTypes}${this.character.hairTypes}${this.character.hairColors}.png`
    }
  },
  methods: {
    saveCharacter () {
      // localStorage.setItem('character', JSON.stringify(this.character))
    },

    changeGender (value) {
      this.character.gender = value,
      this.saveCharacter()
    },

    changeState (value) {
      this.activeState = value
      localStorage.setItem('activeState', value)
    },

    changeHandlerByType (item, type) {
      this[type].forEach(item => item.isActive = false)
      this.character[type] = item.id

      item.isActive = true
      this.saveCharacter()
    },

    nextColor (type) {
      // find index current active item
      const activeItemIndex = this[type].findIndex(item => item.isActive === true)
      // find last index
      const lastItemIndex = this[type].length - 1
      // set isActive = false for all items
      this[type].forEach(item => item.isActive = false)

      if (activeItemIndex === lastItemIndex) {
        this[type][0].isActive = true
        this.character[type] = this[type][0].id
      } else {
        this[type][activeItemIndex + 1].isActive = true
        this.character[type] = this[type][activeItemIndex + 1].id
      }
      // save character
      this.saveCharacter()
    },

    prevColor (type) {
      // find index current active item
      const activeItemIndex = this[type].findIndex(item => item.isActive === true)
      // find last index
      const lastItemIndex = this[type].length - 1
      // set isActive = false for all items
      this[type].forEach(item => item.isActive = false)

      if (activeItemIndex === 0) {
        this[type][lastItemIndex].isActive = true
        this.character[type] = this[type][lastItemIndex].id
      } else {
        this[type][activeItemIndex -1].isActive = true
        this.character[type] = this[type][activeItemIndex - 1].id
      }
      // save character
      this.saveCharacter()
    },

    nextStep (step) {
      this.step = step
      localStorage.setItem('step', step)
      localStorage.setItem('activeState', this.activeState)
      this.saveCharacter()
    },

    toStep (step) {
      this.step = step
      this.saveCharacter()
    }
  }
})
