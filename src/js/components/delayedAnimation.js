module.exports = {
  functional: true,
  render(createElement, { props, data, children }) {
    let delay = props.delay || 150
    let tag = props.tag || "div"
    let animation = props.animation || "animation-fromRight"

    if (children) {
      children.forEach((child, index) => {
        child.data.staticStyle = {
          opacity: 0,
          animationFillMode: "forwards",
          animationDelay: index * delay + "ms"
        }
        child.data.staticClass += " " + animation
      })
    }
    return createElement(tag, { class: 'd-flex' }, children)
  }
}
